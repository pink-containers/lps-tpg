#!../../bin/linux-x86_64/tpg

## You may have to change tpg to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/tpg.dbd"
tpg_registerRecordDeviceDriver pdbbase

#epicsEnvSet ("STREAM_PROTOCOL_PATH", "${TOP}/db")
epicsEnvSet ("STREAM_PROTOCOL_PATH", ".")
epicsEnvSet ("ASYNPORT", "L0")
#epicsEnvSet ("DEVIP", "127.0.0.1")
#epicsEnvSet ("DEVPORT", "50123")
#epicsEnvSet ("IOCBL", "LPQ")
#epicsEnvSet ("IOCDEV", "tpg1")

drvAsynIPPortConfigure("${ASYNPORT}", "${DEVIP}:${DEVPORT}",0,0,0)

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(IOCBL):,R=$(IOCDEV):asyn,PORT=${ASYNPORT},ADDR=0,IMAX=256,OMAX=256")
dbLoadRecords("db/devTPG366.db" "BL=$(IOCBL),TPG366=$(IOCDEV),PORT=$(ASYNPORT)")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
