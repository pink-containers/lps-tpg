# lps-tpg

Container with EPICS IOC for TU LPQ Maxigauge TPG366

## Docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-containers/lps-tpg/lpstpg:latest
    container_name: tpg1
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: “/EPICS/IOCs/lps-tpg/iocBoot/ioctpg”
    command: "./tpg.cmd"
    volumes:
      - type: bind
        source: “/EPICS/autosave/tpg1”
        target: “/EPICS/autosave”
    environment:
      - DEVIP=0.0.0.0
      - DEVPORT=8000
      - IOCBL=LPQ
      - IOCDEV=tpg1

```
